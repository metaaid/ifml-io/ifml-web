import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title =
    'IFML Editor Online - A web based toolbox for creating and editing IFML diagrams';
  error: string = "";
  xmi: string = "";
  draggedFiles: any;
  editorOpen = false;

  openFile(event: any) {
    return this.openDiagramFromFile(event.target.files[0]);
  }

  openEditor(xmi:string = "") {
    this.xmi = xmi;
    this.editorOpen = true;
  }

  openDiagramFromFile(file: File) {
    let reader = new FileReader();
    reader.readAsText(file);

    reader.onload = () => {
      let xmi = reader.result as string;
      this.openEditor(xmi)
    };

    reader.onerror = function() {
      console.log(reader.error);
    };
  }

  dropFile(evt: any){
    return this.openDiagramFromFile(evt[0]);
  }
}
