import { Component, Directive, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

import { Modeler } from 'ifml-js';

@Directive({ selector: 'pane' })
export class Pane {
  @Input() id!: string;
}

@Component({
  selector: 'app-modeler',
  templateUrl: './modeler.component.html',
  styleUrls: ['./modeler.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModelerComponent {
  @Input() xmi: string = '';
  @Output("openFile") openFile: EventEmitter<any> = new EventEmitter();


  ngOnChanges(changes: any) {
    this.openDiagram(changes["xmi"]["currentValue"]);
  }
  hasErrors: boolean = false;
  diagramLoaded: boolean = false;
  errorMessage: string = '';
  newDiagram: string = `<?xml version="1.0" encoding="UTF-8"?>
  <xmi:XMI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ifml="http://www.omg.org/spec/IFML/20140301"
    xmlns:ifmldi="http://www.omg.org/spec/IFML/20130218/IFML-DI"
    xmlns:dc="http://www.omg.org/spec/DD/20100524/DC"
    xmlns:xmi="http://www.omg.org/spec/XMI/20131001">
    <ifml:IFMLModel>
      <interactionFlowModel xmi:type="ifml:InteractionFlowModel" xmi:id="_myModel">
      </interactionFlowModel>
    </ifml:IFMLModel>
    <ifmldi:IFMLDiagram modelElement="_myModel">
    </ifmldi:IFMLDiagram>
  </xmi:XMI>`;

  modeler: Modeler | null = null;

  getDomainModel() {
    var domainJson = {
      name: 'TodoApp',
      uri: './todo.xmi',
      prefix: 'todo',
      associations: [],
      types: [
        {
          name: 'Todo',
          type: 'uml:Classifier',
          properties: [
            {
              name: 'name',
              type: 'String',
            },
            {
              name: 'done',
              type: 'Boolean',
            },
          ],
        },
        {
          name: 'TodoItem',
          type: 'uml:Classifier',
          id: '_v4leFLupEeum0-AyhTWQhw',
          properties: [],
        },
      ],
    };
    return domainJson;
  }

  createModeler(){
    var domainJson = this.getDomainModel();
    this.modeler = new Modeler({
      container: '#js-canvas',
      propertiesPanel: {
        parent: '#js-properties-panel',
      },
      moddleExtensions: [domainJson],
    });
  }

  async openDiagram(xml: string = '') {
    if (!this.modeler) {
      this.createModeler();
    }

    this.diagramLoaded = false;
    if (xml == '') {
      xml = this.newDiagram;
    }

    try {
      var x = await this.modeler?.importXML(xml);
      this.hasErrors = false;
      this.diagramLoaded = true;
    } catch (err: any) {
      this.hasErrors = true;
      this.errorMessage = err["message"];
      console.error(err);
    }
  }

  async saveSVG() {
    var result = await this.modeler?.saveSVG();
    if (result) {
      this.downloadThrughLink('diagram.svg', result['svg']);
    }
  }

  async saveIfml() {
    var result = await this.modeler?.saveXML({
      format: true,
    });
    if (result) {
      this.downloadThrughLink('diagram.ifml', result['xml']);
    }
  }

  downloadThrughLink(name: string, encodedData: string) {
    const link = document.createElement('a');
    link.href = 'data:application/ifml-xml;charset=UTF-8,' + encodedData;
    link.download = name;
    link.click();
  }
}
