declare module "ifml-js" {
  class Viewer {
      importXML(xml: string): void;
  }
  interface SaveXMLResult {
      xml: string;
  }
  interface SaveSVGResult {
      svg: string;
  }
  interface ImportXMLResult {
      warnings: string[];
  }
  class Modeler {
      constructor(options: Object);
      importXML(xml: string): Promise<ImportXMLResult>;
      createDiagram(): Promise<void>;
      on(event: string, callback: Function): void;
      saveSVG(options?: Object): Promise<SaveSVGResult>;
      saveXML(options?: Object): Promise<SaveXMLResult>;
  }
}
declare module '!raw-loader!*' {
  const contents: string;
  export = contents;
}
